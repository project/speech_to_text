# Speech to Text

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

This module provides speech to text for input & Text area fields. 

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the speech_to_text module to the modules directory.

 2. Enable the 'speech_to_text' module and desired sub-modules in 'Extend'.
   (/admin/modules)

CONFIGURATION
-------------

 * Configure your drupal input selectors in the following page.
    ```sh
    /admin/config/systems/speech-to-text
    ```
