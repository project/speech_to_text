(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.speech_to_text = {
        attach: function (context, settings) {
            "use strict";

            var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
            var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

            var recognition = new SpeechRecognition();
            recognition.continuous = false;
            recognition.lang = drupalSettings.speech_to_text.language;
            recognition.interimResults = false;
            recognition.maxAlternatives = 1;

            recognition.onresult = function(event) {
              var transcript = event.results[0][0].transcript;
              if(drupalSettings.speech_to_text.replace) {
                document.activeElement.value = transcript;
              }
              else {
                if(document.activeElement.value) {
                  document.activeElement.value = document.activeElement.value+" "+transcript; 
                }
                else {
                  document.activeElement.value = transcript;
                }
              }
            }
          
            recognition.onend = function(event) {
              $('#'+document.activeElement.id).parent().find('.speech-to-text-icon').html(`<img src="${drupalSettings.speech_to_text.module_path}/svg/mic_mute.svg" alt="svg">`);
            }

            recognition.onnomatch = function(event) {
              console.log("I didn't recognise what your need.");
            }

            recognition.onerror = function(event) {
              console.log('Error occurred in recognition: ' + event.error);
            }

            drupalSettings.speech_to_text.selectors.forEach(element => {
              var $parent=$(element).parent();
              if (!$parent.find('.speech-to-text-icon').length >0){
                $($parent).css({'position':'relative', 'width':'fit-content'});
                $($parent).append(`<div class="speech-to-text-icon"><img src="${drupalSettings.speech_to_text.module_path}/svg/mic_mute.svg" alt="svg"></div>`);
                if($(element).height() <= 30){
                  $($parent).find('.speech-to-text-icon').css({'position':'absolute', 'right':'15px', 'top':'50%'});
                } else {
                  $($parent).find('.speech-to-text-icon').css({'position':'absolute', 'right':'15px', 'bottom':'20px'});
                }
                
                $($parent).find('.speech-to-text-icon').click(function(){
                  $(this).html(`<img src="${drupalSettings.speech_to_text.module_path}/svg/mic.svg" alt="svg">`);
                  $(element).focus();
                  recognition.start();

                  $(element).focusout(function(){
                    $($parent).find('.speech-to-text-icon').html(`<img src="${drupalSettings.speech_to_text.module_path}/svg/mic_mute.svg" alt="svg">`);
                    recognition.stop();
                  });
                });
              };
            });
        }
    }
})(jQuery, Drupal, drupalSettings);