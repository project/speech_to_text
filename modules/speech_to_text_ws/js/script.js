(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.speech_to_text_ws = {
        attach: function (context, settings) {
            "use strict";
            var debug = true;
            console.log(drupalSettings.speech_to_text);
            var sttService = new WebsocketService({
                name: 'speech_to_text',
                debug: debug,
                onopen: function () {
                    // sttService.send("Hello");
                },
                onmessage: function (message) { 
                    console.log(message);
                      if(drupalSettings.speech_to_text.replace) {
                        document.activeElement.value = message;
                      }
                      else {
                        if(document.activeElement.value) {
                          document.activeElement.value = document.activeElement.value+" "+message; 
                        }
                        else {
                          document.activeElement.value = message;
                        }
                      }
                    console.log(message);
                },
                onerror: function (e) {
                    if (debug) {
                        console.log('Websocket chat: ' + e.type);
                    }
                },
                onclose: function (e) {
                    if (debug) {
                        console.log('Websocket chat: ' + e.type);
                    }
                }
            });
            navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
                var recorder = new RecordRTC(stream, {
                    type: 'audio',
                    mimeType: 'audio/webm;codecs=pcm', // endpoint requires 16bit PCM audio
                    recorderType: StereoAudioRecorder,
                    timeSlice: 5000, // set 2000 ms intervals of data
                    desiredSampRate: 16000,
                    numberOfAudioChannels: 1, // real-time requires only one channel
                    bufferSize: 4096,
                    audioBitsPerSecond: 128000,
                    ondataavailable: (blob) => {
                        const reader = new FileReader();
                        reader.onload = () => {
                            const base64data = reader.result;
                            console.log(base64data.split('base64,')[1]);
                            // audio data must be sent as a base64 encoded string
                            if (sttService) {

                    console.log(base64data);
                                sttService.send(JSON.stringify({ audio_data: base64data.split('base64,')[1] }));
                            }
                        };
                        reader.readAsDataURL(blob);
                    },
                });


                drupalSettings.speech_to_text.selectors.forEach(element => {
                  var $parent=$(element).parent();
                  if (!$parent.find('.speech-to-text-icon').length >0){
                    $($parent).css({'position':'relative', 'width':'fit-content'});
                    $($parent).append(`<div class="speech-to-text-icon"><img src="${drupalSettings.speech_to_text.module_path}/svg/mic_mute.svg" alt="svg"></div>`);
                    if($(element).height() <= 30){
                      $($parent).find('.speech-to-text-icon').css({'position':'absolute', 'right':'15px', 'top':'50%'});
                    } else {
                      $($parent).find('.speech-to-text-icon').css({'position':'absolute', 'right':'15px', 'bottom':'20px'});
                    }
                    
                    $($parent).find('.speech-to-text-icon').click(function(){
                      $(this).html(`<img src="${drupalSettings.speech_to_text.module_path}/svg/mic.svg" alt="svg">`);
                      $(element).focus();
                      console.log("Record Started");
                      recorder = new RecordRTC(stream, {
                            type: 'audio',
                            mimeType: 'audio/webm;codecs=pcm', // endpoint requires 16bit PCM audio
                            recorderType: StereoAudioRecorder,
                            timeSlice: 5000, // set 2000 ms intervals of data
                            desiredSampRate: 16000,
                            numberOfAudioChannels: 1, // real-time requires only one channel
                            bufferSize: 4096,
                            audioBitsPerSecond: 128000,
                            ondataavailable: (blob) => {
                                const reader = new FileReader();
                                reader.onload = () => {
                                    const base64data = reader.result;
                                    console.log(base64data.split('base64,')[1]);
                                    // audio data must be sent as a base64 encoded string
                                    console.log(sttService);
                                    if (sttService) {
                                        sttService.send(JSON.stringify({ audio_data: base64data.split('base64,')[1] }));
                                    }
                                };
                                reader.readAsDataURL(blob);
                            },
                        });
                      recorder.startRecording();

                      $(element).focusout(function(){
                        $($parent).find('.speech-to-text-icon').html(`<img src="${drupalSettings.speech_to_text.module_path}/svg/mic_mute.svg" alt="svg">`);
                        recorder.stopRecording();
                      });
                    });
                  };
                });
            })

        }
    }
})(jQuery, Drupal, drupalSettings);