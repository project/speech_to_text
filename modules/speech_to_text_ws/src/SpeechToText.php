<?php

namespace Drupal\speech_to_text_ws;

use Drupal\ai\OperationType\GenericType\AudioFile;
use Drupal\ai\OperationType\SpeechToText\SpeechToTextInput;
use Drupal\ai\OperationType\SpeechToText\SpeechToTextOutput;
use Drupal\websocket\DrupalAwareService;
use Drupal\websocket\DrupalSessionProvider;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

/**
 * {@inheritdoc}
 */
class SpeechToText extends DrupalAwareService implements MessageComponentInterface {

  /**
   * The clients object.
   *
   * @var \SplObjectStorage
   */
  protected $clients;


  /**
   * The ai Provider.
   */
  protected $aiProvider;

  /**
   * Chat constructor.
   *
   * @param string $serviceName
   *   Service name parameter.
   */
  public function __construct($serviceName) {
    parent::__construct($serviceName);
    $this->clients = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function onOpen(ConnectionInterface $conn) {

    // Get user.
    $user = DrupalSessionProvider::getUser($conn);

    // Check permission and close connection if not allowed.
    if (!$this->access($user)) {
      $conn->close();
      return;
    }
    $this->clients->attach($conn);
    echo "New connection {$conn->resourceId}\n";
  }

  /**
   * {@inheritdoc}
   */
  public function onMessage(ConnectionInterface $from, $msg) {
    $aiProvider = \Drupal::service('ai.provider');
    // $user = DrupalSessionProvider::getUser($from);
    $providerInfo = $aiProvider->getDefaultProviderForOperationType('speech_to_text');
    $data = json_decode($msg, TRUE);
    $audio = base64_decode($data['audio_data']);
    $providers = $aiProvider->getProvidersForOperationType('speech_to_text');
    $provider = $aiProvider->createInstance($providerInfo['provider_id']);
    $audio_file = new AudioFile($audio, "audio/wav", "audio.wav");
    $raw_file = new SpeechToTextInput($audio_file);
    try {
      $response = $provider->speechToText($raw_file, $providerInfo['model_id']);
      if ($response instanceof SpeechToTextOutput) {
        $result = $response->getNormalized();
        $from->send($result);
      }
    }
    catch (\Exception $e) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onClose(ConnectionInterface $conn) {
    $this->clients->detach($conn);

    echo "Connection {$conn->resourceId} has disconnected\n";
  }

  /**
   * {@inheritdoc}
   */
  public function onError(ConnectionInterface $conn, \Exception $e) {
    echo "An error occured {$e->getMessage()}\n";

    $conn->close();
  }

}
