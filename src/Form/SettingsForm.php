<?php

namespace Drupal\speech_to_text\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Selectors settings for speech recognition.
 *
 * @package Drupal\speech_to_text\Form
 *
 * @ingroup speech_to_text
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'speech_to_text_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['speech_to_text.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('speech_to_text.settings');

    $basePath = base_path();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $modulePath = \Drupal::service('extension.list.module')->getPath('speech_to_text');
    $dataPath = $host . base_path() . $modulePath . "/data/languages.json";
    $data = file_get_contents($dataPath);
    $languages = json_decode($data, TRUE);

    $form['selectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Selectors'),
      '#description' => $this->t('Enter Selectors to apply speech to text Example: input.field-example .'),
      '#default_value' => $config->get('selectors'),
    ];

    $form['language'] = [
      '#type' => 'select',
      "#options" => $languages,
      '#title' => $this->t('Language'),
      '#description' => $this->t('Select Language for speech recognition.'),
      '#default_value' => $config->get('language') ? $config->get('language') : "en-US",
    ];

    $form['replace'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace text'),
      '#description' => $this->t('Replaces the text all the time'),
      '#default_value' => $config->get('replace'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('speech_to_text.settings');
    $config
      ->set('selectors', trim($form_state->getValue('selectors')))
      ->set('language', $form_state->getValue('language'))
      ->set('replace', $form_state->getValue('replace'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
